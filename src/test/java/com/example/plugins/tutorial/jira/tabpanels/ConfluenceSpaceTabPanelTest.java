package com.example.plugins.tutorial.jira.tabpanels;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.example.plugins.tutorial.jira.tabpanels.ConfluenceSpaceTabPanel;

import static org.mockito.Mockito.*;

/**
 * @since 3.5
 */
public class ConfluenceSpaceTabPanelTest {

    @Before
    public void setup() {

    }

    @After
    public void tearDown() {

    }

    @Test(expected=Exception.class)
    public void testSomething() throws Exception {

        //ConfluenceSpaceTabPanel testClass = new ConfluenceSpaceTabPanel();

        throw new Exception("ConfluenceSpaceTabPanel has no tests!");

    }

}
